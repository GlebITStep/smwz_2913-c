#include <iostream>
using namespace std;

void printDynamicArray(int* arr, int size)
{
	for (int i = 0; i < 5; i++)
	{
		cout << *(arr + i) << endl;
	}
}

void main()
{
	int* ptr = new int[5]{ 11, 22, 33, 44, 55 };
	printDynamicArray(ptr, 5);
	
	//int arr[5] = {11, 22, 33, 44, 55};
	//for (int i = 0; i < 5; i++)
	//{
	//	cout << arr[i] << endl;
	//}

	//cout << endl;
	//
	//int* ptr = new int[5] { 11, 22, 33, 44, 55 };
	//for (int i = 0; i < 5; i++)
	//{
	//	cout << *(ptr + i) << endl;
	//}
}



























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	////static array
//	//int arr[5] = {11, 22, 33, 44, 55};
//	//cout << arr << endl;
//	//cout << arr[0] << endl;
//	//arr[2] = 99;
//	//cout << arr[2] << endl;
//	//cout << endl;
//	//
//	////dynamic array
//	//int* ptr = new int[5] {11, 22, 33, 44, 55};
//	//cout << ptr << endl;
//	//cout << ptr[0] << endl;	//cout << *ptr << endl;
//	//ptr[2] = 99;			//*(ptr + 2) = 99;
//	//cout << ptr[2] << endl;	//cout << *(ptr + 2) << endl;
//	//cout << endl;
//	//delete[] ptr;
//}



























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int* ptr;
//
//	while (true)
//	{
//		ptr = new int[10];
//		//do some work...
//		delete[] ptr;
//	}	
//}




























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int* ptr = nullptr;
//	
//	ptr = new int;
//	*ptr = 10;
//	cout << &ptr << endl;
//	//do some work
//	delete ptr;
//
//	ptr = new int;
//	*ptr = 11;
//	//do some work
//	delete ptr;
//
//	ptr = nullptr;
//}



























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int* ptr = nullptr;
//	ptr = new int;
//	*ptr = 10;
//
//	int* ptr2 = nullptr;
//	ptr2 = new int;
//	*ptr2 = 20;
//}

































//#include <iostream>
//using namespace std;
//
//void function(int* pointer);
//
//void main()
//{
//	int number = 10;
//	function(&number);
//	cout << number << endl;
//}
//
//void function(int* pointer)
//{
//	*pointer = 20;
//}