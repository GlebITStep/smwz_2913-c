#include <iostream>			// ввод / вывод данных
#include <cmath>			// математические функции
#include <ctime>			// функции работы со временем
#include <conio.h>			// функции для управления консолью
using namespace std;

void printArray(int arr[], int size);
void fillArray(int arr[], int size);
void fillArray(int arr[], int size, int min, int max);

void main()
{
	srand(time(0));
	
	const int size = 7;
	int arr[size];

	fillArray(arr, size, -100, 100);
	printArray(arr, size);
}

void printArray(int arr[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

void fillArray(int arr[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		arr[i] = rand();
	}
}

void fillArray(int arr[], int size, int min, int max)
{
	for (int i = 0; i < size; ++i)
	{
		arr[i] = rand() % (max - min) + min + 1;
	}
}











//void main()
//{
//	//cmath
//	cout << pow(5, 3) << endl;		//степень
//	cout << sqrt(25) << endl;				//корень
//	cout << round(3.5) << endl;				//округление
//	cout << ceil(3.000001) << endl;			//округление
//	cout << floor(3.99999) << endl;			//округление
//	cout << abs(-5) << endl;					//модуль
//	cout << fmin(5, 7) << endl;		//минимум
//	cout << fmax(5, 7) << endl;		//максимум
//	cout << endl;
//	
//	//ctime
//	int currentTime = time(0);					//время в секундах
//	cout << currentTime << endl;
//	cout << endl;
//	
//	//cstdlib
//	//exit(0);									//выход из программы
//	srand(currentTime);							//начало рандома
//	cout << rand() << endl;						//рандомное число
//	system("cls");						//очистить консоль
//	system("pause");						//сделать паузу
//	cout << endl;								
//	
//	//conio.h
//	cout << _getch() << endl;					//перехват нажатия клавиши
//	_kbhit();									//нажата ли клавиша
//}


















////Overloaded functions / Перегруженные функции
//
//int sum(int num1, int num2)
//{
//	return num1 + num2;
//}
//
//double sum(double num1, double num2)
//{
//	return num1 + num2;
//}
//
//int sum(int num1, int num2, int num3)
//{
//	return num1 + num2 + num3;
//}
//
//void main()
//{
//	cout << sum(3.9, 6.9) << endl;
//	cout << sum(4, 5) << endl;
//	cout << sum(4, 5, 6) << endl;	
//}






















//void printCube(int number)
//{
//	cout << number * number * number << endl;
//}
//
//int cube(int number)
//{
//	return number * number * number;
//}
//
//void main()
//{
//	//printCube(5);
//	
//	//int result = cube(5);
//	//cout << result << endl;
//	
//	//cout << cube(5) << endl;
//}

