#include <iostream>
#include <ctime>
#include <conio.h>
using namespace std;

void hello()
{
	cout << "Hello!" << endl;
	cout << "My name is Gleb!" << endl;
}

//void drawLine()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		cout << "*";
//	}
//	cout << endl;
//}

//void drawLine(int length)
//{
//	for (int i = 0; i < length; i++)
//	{
//		cout << "*";
//	}
//	cout << endl;
//}

void drawLine(int length, char symbol)
{
	for (int i = 0; i < length; i++)
	{
		cout << symbol;
	}
	cout << endl;
}

//void sum(int num1, int num2)
//{
//	cout << num1 << " + " << num2 << " = " << num1 + num2 << endl;
//}

//void sum(int num1, int num2)
//{
//	cout << num1 + num2;
//}

int sum(int num1, int num2)
{
	return num1 + num2;
}


int power(int num, int pow)
{
	int result = 1;
	for (int i = 0; i < pow; i++)
	{
		result *= num;
	}
	return result;
}

void main()
{
	int number, pow;
	cout << "Enter number: ";
	cin >> number;
	cout << "Enter power: ";
	cin >> pow;
	int result = power(number, pow);
	cout << "Result: " << result;
	drawLine(result, '*');




	//int result = power(5, 3);
	//cout << result;




	//int result = sum(3, 5);




	//cout << "BEGIN!";
	//hello();
	//drawLine(5, '-');
	//int result = sum(3, 5);
	//cout << result * 10 << endl;



	//drawLine(8, '*');
	//drawLine(5, '_');
	//cout << "Hello!" << endl;
	//drawLine(10, char(219));
}