#include <iostream>
using namespace std;

// прототипы функций
int cube(int number);
void drawLine(int length = 10, char symbol = '*');

void main()
{
	drawLine(2, '+');
	drawLine(5);
	drawLine();
	cout << cube(5) << endl;
}

int cube(int number)
{
	return number * number * number;
}

void drawLine(int length, char symbol)
{
	for (int i = 0; i < length; i++)
	{
		cout << symbol;
	}
	cout << endl;
}