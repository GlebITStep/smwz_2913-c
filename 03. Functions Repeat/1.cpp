#include <iostream>
using namespace std;

// void - пустота

void function() // сигнатура функции (тип возвращаемого значения, название, список параметров)
{				
	// тело функции
}

void hello()
{
	cout << "Hello" << endl;
}

void drawLineOfStars(int length)
{
	for (int i = 0; i < length; i++)
	{
		cout << "*";		
	}
	cout << endl;
}

void drawLine(int length, char symbol)
{
	for (int i = 0; i < length; i++)
	{
		cout << symbol;
	}
	cout << endl;
}

int cube(int number)
{
	int result = number * number * number;
	return result;
}

int sum(int firstNumber, int secondNumber)
{
	return firstNumber + secondNumber;
}

void main()
{
	cout << cube(sum(cube(2), cube(3))) << endl;

	
	//int result = sum(4, 6);
	//int finalResult = cube(result);
	//cout << finalResult << endl;


	
	//int answer = sum(5, 7);
	//cout << answer << endl;


	
	//int answer = cube(5);
	//cout << answer * 10 << endl;


	
	//drawLine(20, '-');
	//hello(); // вызов функции
	//drawLineOfStars(15);
	//hello();
	//drawLineOfStars(30);
}

