#include <iostream>
using namespace std;

template <typename T> T* addToArray(T* arr, int* size, T data);
template <typename T> T* removeFromArray(T* arr, int* size, int position);
template <typename T> void printArray(T* arr, int size);

template <typename T> T* insertToArray(T* arr, int* size, int position, T data);
template <typename T> int indexOf(T* arr, int size, T data);
template <typename T> int lastIndexOf(T* arr, int size, T data);

void main()
{
	int size = 5;
	char* arr = new char[size] { 'A', 'B', 'C', 'D', 'E' };

	printArray(arr, size); //11 22 33 44 55

	arr = addToArray(arr, &size, 'Z');

	printArray(arr, size); //11 22 33 44 55 99

	arr = removeFromArray(arr, &size, 2);

	printArray(arr, size); //11 22 44 55 99
}

template <typename T> T* removeFromArray(T* arr, int* size, int position)
{
	T* tmp = new T[*size - 1];
	for (int i = 0; i < position; ++i)
	{
		tmp[i] = arr[i];
	}
	for (int i = position + 1; i < *size; ++i)
	{
		tmp[i - 1] = arr[i];
	}
	delete[] arr;
	arr = tmp;
	(*size)--;
	return arr;
}

template <typename T> T* addToArray(T* arr, int* size, T data)
{
	T* tmp = new T[*size + 1];
	for (int i = 0; i < *size; ++i)
	{
		tmp[i] = arr[i];
	}
	delete[] arr;
	arr = tmp;
	arr[*size] = data;
	(*size)++;
	return arr;
}

template <typename T> void printArray(T* arr, int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << arr[i] << '\t';
	}
	cout << endl;
}

































//
//
//#include <iostream>
//using namespace std;
//
//int* addToArray(int* arr, int* size, int data);
//int* removeFromArray (int* arr, int* size, int position);
//void printArray(int* arr, int size);
//
//int* insertToArray(int* arr, int* size, int position, int data);
//int indexOf(int* arr, int size, int data);
//int lastIndexOf(int* arr, int size, int data);
//
//
//void main()
//{
//	int size = 5;
//	int* arr = new int[size] { 11, 22, 33, 44, 55 };
//
//	printArray(arr, size); //11 22 33 44 55
//
//	arr = addToArray(arr, &size, 99);
//	
//	printArray(arr, size); //11 22 33 44 55 99
//
//	arr = removeFromArray(arr, &size, 2);
//
//	printArray(arr, size); //11 22 44 55 99
//}
//
//int* removeFromArray(int* arr, int* size, int position)
//{
//	int* tmp = new int[*size - 1];
//	
//	for (int i = 0; i < position; ++i)
//	{
//		tmp[i] = arr[i];
//	}
//
//	for (int i = position + 1; i < *size; ++i)
//	{
//		tmp[i - 1] = arr[i];
//	}
//
//	delete[] arr;
//
//	arr = tmp;
//
//	(*size)--;
//
//	return arr;
//}
//
//int* addToArray(int* arr, int* size, int data)
//{
//	int* tmp = new int[*size + 1];
//	
//	for (int i = 0; i < *size; ++i)
//	{
//		tmp[i] = arr[i];
//	}
//	
//	delete[] arr;
//	
//	arr = tmp;
//	
//	arr[*size] = data;
//	
//	(*size)++;
//	
//	return arr;
//}
//
//void printArray(int* arr, int size)
//{
//	for (int i = 0; i < size; ++i)
//	{
//		cout << arr[i] << '\t';
//	}
//	cout << endl;
//}
//




























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int* arr = new int[5]{ 11, 22, 33, 44, 55 };
//
//	
//	//1 - Выделить новую память
//	int* tmp = new int[6];
//
//	//2 - Перенести данные из старой памяти в новую
//	for (int i = 0; i < 5; ++i)
//	{
//		tmp[i] = arr[i];
//	}
//
//	//3 - Очистить старую память
//	delete[] arr;
//
//	//4 - Записать в указаель адрес нового массива
//	arr = tmp;
//}































//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int x = 10;
//
//	int* arr = new int[5] { 11, 22, 33, 44, 55 };
//
//	int y = 20;
//}




















//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int x = 10;
//	
//	int arr[5]{ 11, 22, 33, 44, 55 };
//
//	int y = 20;
//}

























//#include <iostream>
//using namespace std;
//
//void main()
//{
//	int* ptr;
//	ptr = new int[1000];
//	
//	// do some work
//
//	delete[] ptr;
//}





















//#include <iostream>
//using namespace std;
//
//void function(int* ptr)
//{
//	*ptr = 20;
//}
//
//void main()
//{
//	int* ptr;
//
//	int num = 10;
//	function(&num);
//	cout << num << endl;
//}