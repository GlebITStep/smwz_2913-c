#include <iostream>
#include <ctime>
using namespace std;

template <typename T> void printArray(T arr[], int size);
template <typename T> void fillArray(T arr[], int size, T min, T max);
template <typename T> int linearSearch(T arr[], int size, int dataToSearch);
template <typename T> int binarySearch(T arr[], int size, int dataToSearch);
template <typename T> T maximum(T arr[], int size);
template <typename T> T minimum(T arr[], int size);
template <typename T> void bubbleSort(T arr[], int size);

void main()
{
	//LINEAR SEARCH
	//int arr[] = { 11, 22, 33, 44, 55, 66, 77, 88, 99 };
	//int result = linearSearch(arr, 9, 78);
	//if (result >= 0)
	//	cout << "YES! Position: " << result << endl;
	//else
	//	cout << "NO!";
	
	////BINARY SEARCH
	//const int size = 10;
	//int arr[size] = { 2, 5, 8, 12, 16, 23, 38, 56, 72, 91 };
	//cout << binarySearch(arr, size, 78) << endl;

	////MAXIMUM
	//const int size = 10;
	//int arr[size] = { 16, 38, 8, 5, 23, 72, 91, 2, 56, 12 };
	//cout << maximum(arr, size) << endl;
	
	////MINIMUM
	//const int size = 10;
	//int arr[size] = { 16, 38, 8, 5, 23, 72, 91, 2, 56, 12 };
	//cout << minimum(arr, size) << endl;

	////BUBBLE SORT
	//const int size = 15;
	//int arr[size] = { 3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48 };
	//printArray(arr, 15);
	//bubbleSort(arr, 15);
	//printArray(arr, 15);


	srand(time(0));	
	const int size = 20;
	int arr[size] = {};
	
	printArray(arr, size);
	
	fillArray(arr, size, 0, 100);
	
	printArray(arr, size);
	
	cout << linearSearch(arr, size, 45) << endl;

	cout << minimum(arr, size) << endl;

	cout << maximum(arr, size) << endl;

	bubbleSort(arr, size);

	printArray(arr, size);

	cout << binarySearch(arr, size, 13) << endl;	
}

template <typename T> void bubbleSort(T arr[], int size)
{
	bool sorted = false;
	while (!sorted)
	{
		sorted = true;
		for (int i = 0; i < size - 1; ++i)
		{
			if (arr[i] > arr[i + 1])
			{
				int temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
				sorted = false;
			}
		}
	}
}

template <typename T> T minimum(T arr[], int size)
{
	T min = arr[0];
	for (int i = 0; i < size; ++i)
	{
		if (arr[i] < min)
			min = arr[i];
	}
	return min;
}

template <typename T> T maximum(T arr[], int size)
{
	T max = arr[0];
	for (int i = 0; i < size; ++i)
	{
		if (arr[i] > max)
			max = arr[i];
	}
	return max;
}

template <typename T> int binarySearch(T arr[], int size, int dataToSearch)
{
	int center = size / 2 - 1;
	int half = size;
	while (half != 0)
	{
		half = half / 2;
		if (arr[center] == dataToSearch)
			return center;
		else if (arr[center] < dataToSearch) //right
			center = center + (half / 2 + 1);
		else if (arr[center] > dataToSearch) //left
			center = center - (half / 2 + 1);
	}
	return -1;
}

template <typename T> int linearSearch(T arr[], int size, int dataToSearch)
{
	for (int i = 0; i < size; ++i)
	{
		if (arr[i] == dataToSearch)
			return i;
	}
	return -1;
}

template <typename T> void fillArray(T arr[], int size, T min, T max)
{
	for (int i = 0; i < size; ++i)
	{
		arr[i] = rand() % (max - min) + min + 1;
	}
}

template <typename T> void printArray(T arr[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

























//template <typename T> void printArray(T arr[], int size);
//
//void main()
//{
//	int arr1[] = { 11, 22, 33, 44, 55 };
//	double arr2[] = { 11.1, 22.2, 33.3, 44.4, 55.5 };
//	char arr3[] = { 'A', 'B', 'C', 'D', 'E' };
//
//	printArray(arr1, 5);
//	printArray(arr2, 5);
//	printArray(arr3, 5);
//}
//
//template <typename T> void printArray(T arr[], int size)
//{
//	for (int i = 0; i < size; ++i)
//	{
//		cout << arr[i] << " ";
//	}
//	cout << endl;
//}
















//// OVERLOADED FUNCTIONS
//
////void sum(int first, int second)
////{
////	cout << first + second << endl;
////}
//
////void sum(double first, double second)
////{
////	cout << first + second << endl;
////}
//
//
////TEMPLATE FUNCTION
//
//template <typename T> T sum(T first, T second)
//{
//	return first + second;
//}
//
//void main()
//{
//	cout << sum(4, 9) << endl;
//	cout << sum(1.9, 4.9) << endl;
//	cout << sum('A', 'B') << endl;
//}
